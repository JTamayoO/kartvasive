#include <SoftwareSerial.h>

uint8_t dataRX[35];//Receive buffer.
uint8_t dataTX[35];//Transmit buffer.
uint8_t _UID[4];// stores the UID (unique identifier) of a card.
uint8_t ATQ[2];//Answer to request


uint8_t global_lenght;

#define init_send  0
#define init_not_send_ack 2
#define init_not_send_wait 4
#define ack_state  1
#define wait_state  3
#define printing_state 5
#define wait_val_empty 0xFF
#define wait_val_ready 0x00
#define pinRx 9
#define pinTx 8

uint8_t nfc_read_val = wait_val_empty;
int current_state = init_send;
SoftwareSerial Control_Serial(pinRx,pinTx);
unsigned char motorState = 0;
unsigned char motorPower = 128;
unsigned long last_tag_found = 0;

void setup() {
    pinMode(4,OUTPUT);  
    pinMode(5,OUTPUT);  
    pinMode(6,OUTPUT);  
    pinMode(7,OUTPUT);
    //Serial.begin(115200);
    Serial1.begin(115200);
    Control_Serial.begin(9600);
    while (!Serial) {
        ;
    }
    configureSAM();
    last_tag_found = millis();
}

void loop() {

    String state = "State: ";
    //Serial.println(state + current_state);

    control();
    
    switch(current_state) {
    case init_send:
        nfc_read_val = 0xFF;
        init(_UID, ATQ);
        current_state = ack_state;
        break;

    case init_not_send_ack:

        if( nfc_buffer_ready() ) {
            current_state = ack_state;
        }

        break;

    case init_not_send_wait:
        if( nfc_buffer_ready() ) {
            current_state = printing_state;
        }

        break;
    case ack_state:

        if(  nfc_read_val == wait_val_ready ) {
            current_state = wait_state;
            for (int i = 0; i < 5 ; i++) {
                dataRX[i] = Serial1.read();
            }
            nfc_read_val = wait_val_empty;
        } else {
            current_state = init_not_send_ack;
        }
        break;
    case wait_state:

        if( nfc_read_val == wait_val_ready ) {
            current_state = printing_state;
            getData(global_lenght);
            for (int i = 17; i < (21) ; i++) {
                _UID[i-17] = dataRX[i];
            }
        } else {
            current_state = init_not_send_wait;
        }

        break;

    case printing_state:      
        if( (millis()-last_tag_found) > 1000){
          Control_Serial.print("1");
          last_tag_found = millis();
          Serial.print("CARD READ\n");
        }
        
        current_state=init_send;
        break;
    }

}
void control(){
  int serialDataI;
  int serialDataD;
  
  if(Control_Serial.available()){  
  
    serialDataI = Control_Serial.readStringUntil(',').toInt();
    //Serial.println(serialDataI);
    serialDataD = Control_Serial.readStringUntil(',').toInt();
    //Serial.println(serialDataD);
 
      if(serialDataI < 0){
       digitalWrite(4,0);
       serialDataI=-serialDataI;
     }else{
         digitalWrite(4,1);
       }
     if(serialDataD < 0){
       digitalWrite(7,0);
       serialDataD=-serialDataD;
       } else{
         digitalWrite(7,1);
       }
     analogWrite(5,serialDataI);
     analogWrite(6,serialDataD);
  }

}

int nfc_buffer_ready() {

    delay(5); //DELaY hace parte de la especificacion 14443-3 no borrar o puede explotar.
    int is_ready = 0;

    if(nfc_read_val != 0x00) {
        nfc_read_val = Serial1.read();

    } else {
        is_ready = 1;
    }

    return is_ready;
}

//**********************************************************************
//!The goal of this command is to detect as many targets (maximum MaxTg)
// as possible in passive mode.
uint8_t init(uint8_t *UID , uint8_t *ATQ) { //! Request InListPassive
    Serial1.flush();
    dataTX[0] = 0x04; // Length
    lengthCheckSum(dataTX); // Length Checksum
    dataTX[2] = 0xD4;
    dataTX[3] = 0x4A; // Code
    dataTX[4] = 0x01; //MaxTarget
    dataTX[5] = 0x00; //BaudRate = 106Kbps
    dataTX[6] = 0x00; // Clear checkSum position
    checkSum(dataTX);
    sendTX(dataTX , 7 ,23);
}

void sendTX(uint8_t *dataTX, uint8_t length, uint8_t outLength) {
    //Serial.println("sendTX");
    Serial1.print(char(0x00));
    Serial1.print(char(0x00));
    Serial1.print(char(0xFF));

    for (int i = 0; i < length; i++) {
        Serial1.print(char(dataTX[i]));
    }

    Serial1.print(char(0x00));

    global_lenght = outLength;
}

void getData(uint8_t outLength) {
    for (int i=5; i < outLength; i++) {
        dataRX[i] = Serial1.read();
    }
}

//Calculates the checksum and stores it in dataTX buffer
void checkSum(uint8_t *dataTX) {
    for (int i = 0; i < dataTX[0] ; i++) {
        dataTX[dataTX[0] + 2] += dataTX[i + 2];
    }
    byte(dataTX[dataTX[0] + 2]= - dataTX[dataTX[0] + 2]);
}

//Calculates the length checksum and sotres it in the buffer.
uint8_t lengthCheckSum(uint8_t *dataTX) {
    dataTX[1] = byte(0x100 - dataTX[0]);
}

//set internal parameters of the PN532,
bool configureSAM(void) { //! Configure the SAM

    Serial1.print("               ");
    dataTX[0] = 0x05; //Length
    lengthCheckSum(dataTX); // Length Checksum
    dataTX[2] = 0xD4;
    dataTX[3] = 0x14;
    dataTX[4] = 0x01; // Normal mode
    dataTX[5] = 0x14; // TimeOUT
    dataTX[6] = 0x00; // IRQ
    dataTX[7] = 0x00; // Clean checkSum position
    checkSum(dataTX);

    sendTX(dataTX , 8, 13);
    delay(100);
}


